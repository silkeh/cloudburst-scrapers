module gitlab.com/silkeh/cloudburst-scrapers

go 1.17

require (
	github.com/plgd-dev/go-coap/v2 v2.5.0
	github.com/silkeh/senml v0.0.0-20210422111947-b6df6a0d383f
	gitlab.com/silkeh/cloudburst v0.0.0-20220211194352-a550a8d2aa02
)

require (
	github.com/dsnet/golib/memfile v1.0.0 // indirect
	github.com/pion/dtls/v2 v2.1.2 // indirect
	github.com/pion/logging v0.2.2 // indirect
	github.com/pion/transport v0.13.0 // indirect
	github.com/pion/udp v0.1.1 // indirect
	github.com/plgd-dev/kit/v2 v2.0.0-20211006190727-057b33161b90 // indirect
	github.com/ugorji/go/codec v1.2.6 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
