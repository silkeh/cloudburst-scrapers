package main

import (
	"bytes"
	"context"
	"flag"
	"log"
	"time"

	"github.com/plgd-dev/go-coap/v2/message"
	"github.com/plgd-dev/go-coap/v2/udp"
	"github.com/silkeh/senml"

	"gitlab.com/silkeh/cloudburst-scrapers/pkg/easyenergy"
)

const (
	defaultBackfill = 6 * time.Hour
	defaultPrefetch = 6 * time.Hour
	requestTimeout  = 10 * time.Second
)

func request(client *easyenergy.Client, server, measurement string, backfill, prefetch time.Duration) {
	m, err := client.GetSenML(time.Now().Add(-backfill), time.Now().Add(prefetch))
	if err != nil {
		log.Printf("Error collecting EasyEnergy data: %s", err)
		return
	}

	b, err := senml.EncodeCBOR(m)
	if err != nil {
		log.Printf("Error serializing CBOR: %s", err)
		return
	}

	conn, err := udp.Dial(server, udp.WithErrors(func(err error) {})) // suppress error
	if err != nil {
		log.Printf("Error creating CoAP client: %s", err)
		return
	}
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	defer cancel()

	_, err = conn.Post(ctx, "api/v1/"+measurement, message.AppCBOR, bytes.NewReader(b))
	if err != nil {
		log.Printf("Error sending CoAP: %s", err)
	}
}

func main() {
	var (
		server, measurement          string
		interval, backfill, prefetch time.Duration
	)

	flag.StringVar(&server, "server", "localhost:5683", "CoAP server")
	flag.StringVar(&measurement, "measurement", "easy_energy", "Measurement name")
	flag.DurationVar(&interval, "interval", 0, "Collection interval, 0 to scape only once")
	flag.DurationVar(&backfill, "backfill", defaultBackfill, "Amount of time to backfill")
	flag.DurationVar(&prefetch, "prefetch", defaultPrefetch, "Amount of time to prefetch")
	flag.Parse()

	client := easyenergy.NewClient(nil)

	request(client, server, measurement, backfill, prefetch)

	if interval == 0 {
		return
	}

	ticker := time.NewTicker(interval)
	for range ticker.C {
		request(client, server, measurement, backfill, prefetch)
	}
}
