// Package buienradar provides a simple client for the Buienradar weather API.
package buienradar

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/silkeh/senml"
)

// Client is a HTTP client for the Buienradar API.
type Client struct {
	*http.Client
}

// NewClient creates a new HTTP client for the Buienradar API.
func NewClient(cli *http.Client) *Client {
	if cli == nil {
		cli = http.DefaultClient
	}

	return &Client{Client: cli}
}

// Get retrieves the latest data from the Buienradar API.
func (c *Client) Get() (*Buienradar, error) {
	b := new(Buienradar)

	resp, err := c.Client.Get(URL) // nolint:noctx
	if err != nil {
		return nil, fmt.Errorf("error retrieving Buienradar data: %w", err)
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(b)
	if err != nil {
		return nil, fmt.Errorf("error decoding Buienradar response: %w", err)
	}

	return b, nil
}

// GetSenML collects measurements from Buienradar and converts them to SenML.
func (c *Client) GetSenML(stations ...string) (measurements []senml.Measurement, err error) {
	timezone, err := time.LoadLocation("Europe/Amsterdam")
	if err != nil {
		return nil, fmt.Errorf("error loading time location: %w", err)
	}

	data, err := c.Get()
	if err != nil {
		return nil, err
	}

	for _, measurement := range data.Actual.Stationmeasurements {
		station := measurement.Stationname[len("Meetstation "):]
		if !contains(stations, station) {
			continue
		}

		t, _ := time.ParseInLocation("2006-01-02T15:04:05", measurement.Timestamp, timezone)
		measurements = append(measurements,
			senml.NewValue(station, measurement.Airpressure, senml.Hectopascal, t, 0),
			senml.NewValue(station, measurement.Temperature, senml.Celsius, t, 0),
			senml.NewValue(station, measurement.Humidity, senml.RelativeHumidityPercent, t, 0),
			senml.NewValue(station+" (ground)", measurement.Groundtemperature, senml.Celsius, t, 0),
			senml.NewValue(station+" (feel)", measurement.Feeltemperature, senml.Celsius, t, 0),
			senml.NewValue(station+" visibility", measurement.Visibility, senml.Meter, t, 0),
			senml.NewValue(station+" wind speed", measurement.Windspeed, senml.MeterPerSecond, t, 0),
			senml.NewValue(station+" precipitation", measurement.Precipitation, senml.Millimeter, t, 0),
			senml.NewValue(station+" sun power", measurement.Sunpower, senml.WattPerSquareMeter, t, 0),
			senml.NewValue(station+" wind direction", measurement.Winddirectiondegrees, senml.Degree, t, 0),
			senml.NewString(station+" icon", measurement.IconEmoji(), senml.None, t, 0),
		)
	}

	return measurements, nil
}
