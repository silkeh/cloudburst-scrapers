package easyenergy

// Error represents an error message from the API.
type Error struct {
	Message string
}

// Error returns the error for this Error.
func (e *Error) Error() string {
	return e.Message
}
