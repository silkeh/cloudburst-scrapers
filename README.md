# Cloudburst Scrapers

This repository includes scrapers that scrape data from various sources,
and push them as JSON encoded SenML over HTTP.

While the scrapers are generic, they are only guaranteed to work with [Cloudburst][].

Build/install all scrapers to your `$GOPATH` using:

```console
go install gitlab.com/silkeh/cloudburst-scrapers/cmd/...@latest
```

## Included scrapers

- The Buienradar scraper scrapes weather data from the [Buienradar API][].
- The EasyEnergy scraper scrapes actual and future energy prices from the [EasyEnergy website][].

[Cloudburst]: https://gitlab.com/silkeh/cloudburst
[Buienradar API]: https://www.buienradar.nl/overbuienradar/gratis-weerdata
[EasyEnergy website]: https://www.easyenergy.com/nl/energietarieven
